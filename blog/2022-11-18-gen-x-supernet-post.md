---
slug: gaia-x-web3-ecosystem
title: Introduction to GEN-X
authors: [deltadao]
tags: [gen-x, supernet, governance]
sidebar_position: 2
---

GEN-X is a community-driven Pan-European network for the Gaia-X Web3 ecosystem. It is owned by no one and open to everyone, entirely run and governed by Gaia-X community members dedicated to the Gaia-X Trust Framework. Early network validators for GEN-X include Arsys (Spain), deltaDAO (Germany), EuProGigant (Austria/Germany), Exoscale (Switzerland/Austria/Germany), IONOS Cloud (Germany), and Wobcom (Germany), with more to follow. 

This decentralized and autonomous Gaia-X Web3 ecosystem will leverage [Polygon Supernets](https://polygon.technology/blog/introducing-polygon-supernets-powered-by-polygon-edge-100m-ecosystem-fund) build with [Polygon Edge](https://polygon.technology/solutions/polygon-edge) to scale an open, secure, and compliant federated digital ecosystem and identify a framework for businesses and institutions adopting and building towards Gaia-X.

As GEN-X is a customizable blockchain based on Polygon Supernet, this enables the web3 ecosystem to build and launch dedicated blockchain networks explicitly tailored to the Gaia-X needs. The modular framework supports a variety of scaling and infrastructure solutions, from sovereign and enterprise EVM (Ethereum Virtual Machine) chains to full-blown Layer 2 solutions. This allows us to develop and improve the network over time and benefit from all developments revolving around GEN-X and Supernets.

# Scalability and EVM Compatability

Scaling and throughput present the biggest challenges for developers with public networks. GEN-X allows fast and smooth scaling even in the case of extreme transaction load, enabling developers to build applications with millions of daily active users and manage tens of millions of transactions per day. An initial [public stress test](https://twitter.com/deltaDAO/status/1601137387760865281) has been performed during the Gaia-X Open Source Community meeting to show the capabilities of the network.

Supernets are modular and configurable, following the modular blockchain design approach (blockchain legos). Instead of a monolithic client architecture, Supernets introduce separate modules for every critical layer of the stack (execution environments, consensus, data availability, networking, etc.). This allows us to utilize modules across different Supernets and benefit from other Polygon scaling solutions.

With GEN-X inbuilt support for EVM, Solidity smart contracts can be easily ported to the GEN-X network without any modifications. Any developer with experience building on Ethereum or EVM-compatible chains can easily do the same with GEN-X, utilizing tools including [Truffle Suite](https://trufflesuite.com/), [Hardhat](https://hardhat.org/), [MetaMask](https://metamask.io/), [Remix](https://remix.ethereum.org/), and [block explorers](https://github.com/blockscout/blockscout). Improvements from other projects utilizing Polygon Supernets can benefit the whole Gaia-X ecosystem and vice versa. Production-ready developer tools and libraries are available from the beginning, speeding up initial development and connecting the Gaia-X Web3 ecosystem to the thriving Ethereum ecosystem.

Supernets infrastructure tooling allows users to launch their dedicated network with just a few lines of code in the configuration file. Every Supernet is built and run for a specific application, project, or use case. Without this tooling, projects would have to fork an Ethereum mainnet node and spend months developing custom modifications to fit their business needs.

# Blockchain Lego

Supernets can utilize different scaling architectures. "Blockchain legos," i.e., architectural options offered by Polygon Edge, will remain available through Supernets. Supernets make it easier to operate such a system, as maintenance is straightforward. Additionally, any Supernet can change its underlying architecture at any point. So it is possible to start with Proof of Authority and switch to Proof of Stake or other mechanisms once available and the network participants are ready.

GEN-X's initial aim is to provide a high-performance, low-cost and interoperable alternative to other networks, such as Ethereum, to speed up the launch of a dedicated Gaia-X Supernet. This allows us to focus on the following properties without giving up any flexibility during development:

- Scalability: The GEN-X Testnet uses a modular architecture that allows for high transaction throughput and low transaction fees, making it well-suited for high-volume applications such as data economy and industry 4.0.
- Interoperability: The GEN-X Testnet is designed to be interoperable with other EVM-compatible networks, allowing for easy transfer of assets and data between different blockchain networks. This enables developers to build cross-chain applications and allows users to access a broader range of services and applications.
- Security: The GEN-X Testnet can connect to the Ethereum mainnet to provide the security and robustness of Ethereum once needed. This helps to ensure the safety and integrity of the network and its applications. But the network can also be fully independent and other Gaia-X networks can use GEN-X as their base layer to connect to each other. The grade of security can adjust over time based on current needs.
- Flexibility: The GEN-X Testnet is highly flexible, allowing developers to choose from various protocols and technologies, such as Ethereum Virtual Machine (EVM) compatible chains, plasma chains, and zk-rollups, to build their applications. Further modules are currently under development and can be developed in parallel by independent teams. This enables developers to build applications tailored to their specific needs and requirements.

# Shared Security and a Gaia-X Supernet

Shared security is a distinctive feature of Supernet-based networks, allowing use case-specific chains to benefit from the economic security that a network like GEN-X could provide in the future. This differs from interchain protocols that use bridges, where each chain is considered sovereign and must maintain its own validator set and economic security. One concern with these protocols is that they may not be scalable from a security perspective. For example, suppose a blockchain network is designed to scale using its own tokenomics. In that case, the lower market cap networks may have less economic security attached to them and be more vulnerable to attacks. The GEN-X network addresses these concerns by providing strong security guarantees at the genesis and allowing the use case specific chains to tap into the security of the GEN-X network. This means that sovereign chains do not need to expend as much effort to grow the value of their coin to achieve sufficient security against attackers.

A shared security layer offers several advantages compared to traditional blockchain architectures. Some further vital benefits of a shared security layer include the following:

- Improved scalability: A shared security layer allows multiple blockchain networks to share the same underlying security infrastructure, reducing the computing power and resources required to secure the network. This can improve the scalability of the network, allowing for higher transaction throughput and lower transaction fees.
- Enhanced interoperability: A shared security layer can also facilitate interoperability between different blockchain networks, allowing for the easy transfer of assets and data between different networks. This can enable the development of cross-chain applications and services and give users access to a broader range of functionality.
- Increased security: By sharing a common security layer, different blockchain networks can benefit from the collective security of the entire network. This can help to protect against attacks and ensure the integrity and safety of the network and its applications.
- Flexibility: A shared security layer can also provide flexibility, allowing different networks to choose from various protocols and technologies to secure their networks. This enables networks to tailor their security to their specific needs and requirements.

Overall, a shared security layer can provide many benefits, including improved scalability, enhanced interoperability, increased security, and greater flexibility. So we envision a Gaia-X Supernet that connects use case-specific networks to create a decentralized data economy. More on this in a dedicated article in the future.

# Public Governance

Governance in a DLT network is essential to the efficient operation of the network and for enabling the network to adapt to changing conditions and the needs of its participants.

One of the critical features of DLT governance is that it is decentralized, meaning that decisions are made by the network as a whole rather than by a central authority. This allows for greater transparency and fairness in the decision-making process following Gaia-X principles, as all participants in the network have an equal say in the direction of the network.

There are several approaches to blockchain governance, each of which has unique characteristics and advantages. One common method is to use decentralized autonomous organizations (DAOs). These autonomous entities operate on the blockchain and are governed by smart contracts, as described in the [Gaia-X Architecture Documents](https://gaia-x.eu/mediatech/publications/). These organizations allow for decentralized decision-making and can enable the network to adapt and evolve quickly in response to changing conditions.

Another approach to blockchain governance is on-chain voting, where network participants can vote on specific proposals and decisions. This allows for democratic decision-making within the network, as all participants have an equal opportunity to have their voices heard.

Overall, effective governance is crucial for the success of any blockchain network, as it enables the network to adapt and evolve in response to the needs of its users. By decentralizing decision-making and using mechanisms such as DAOs and on-chain voting, blockchain networks can ensure that they are operated transparently and fairly, leading to greater trust and adoption among users.

Currently, as the GEN-X network is operated as a Proof of Authority network, regular meetings of the validators are happening to grow the list of validators further. Voting happens on-chain at this point. Once we have enough validators, we will transition to the next phase. At this point, we can establish a GEN-X DAO to govern off-chain decisions like updates in the Trust Framework or strategy decisions.

# Joining GEN-X

Suppose you are interested in joining our GEN-X network and contributing to the first federated Gaia-X ecosystem with community-driven governance. In that case, you are welcome to contact us (<contact@delta-dao.com>)) or any other partner from the network. As a prerequisite, you must commit to the Gaia-X Trust Framework and display W3C-compliant verifiable credentials for your identity and infrastructure. These Gaia-X complaint Self-Descriptions allow other participants of the Gaia-X Web3 ecosystem to decide if they trust you and your services.

If you want to learn more about Gaia-X compliance and the Gaia-X Trust Framework, you will find a great starting point [here](https://compliance.gaia-x.eu/). You can also read the basic technical requirements, including [step-by-step](https://gitlab.com/gaia-x/lab/compliance/gx-compliance) instructions on creating Gaia-X-compliant self-descriptions.

When all conditions are met, any validator can initiate a vote to add a new member to the GEN-X network. The current validators then vote democratically on whether to accept the new participant. Furthermore, we can recommend our [blog post](https://gitlab.com/gaia-x/lab/compliance/gx-compliance) on the Gaia-X Compliance Service, which gives a detailed overview of Gaia-X's signing and validation services.

# Conclusion and Next Steps

In conclusion, GEN-X is an integral part of the Gaia-X Web3 ecosystem. Its use of Polygon Supernets will enable it to provide scalable, interoperable, and compliant blockchain networks that support various applications and use cases. By leveraging the Polygon Supernet's strengths, GEN-X will deliver high-performance, low-cost, and flexible solutions that help the growth and development of the Gaia-X ecosystem.

Regular governance meetings and public and transparent communication about developments will attract more developers to the Gaia-X community and connect to the broader web3 (developer) community to enhance and improve the GEN-X network to build toward production.
