---
sidebar_position: 4
title: Participants
---


## List of Participants (WIP)

| Participant                         | Public Address                             | Status        | Gaia-X Participant Self-Description                                  |
| ----------------------------------- | ------------------------------------------ | ------------- | -------------------------------------------------------------------- |
| deltaDAO AG                         | 0x628677D9A9d93a913182fa04893Da0ce4E6570Ee | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x8d4198E9af22863d4269dDA6a41eF2BfA187AbAc | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xC0E3B447c1e7B22769952E89389Ef2cD9B812Cc5 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xBf252dD5b3a31A50Db34113e12517b21D143AC52 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x0bd21cF4Da78f74c483a1109ac3A30794FBd556B | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x9c26685b6E8e2997d9aAf3f1a642f1b1b3dB9580 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xd0ea08826FA10eEaA3871a6AE680E5f15149F355 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x4C84a36fCDb7Bc750294A7f3B5ad5CA8F74C4A52 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x68C24FA5b2319C81b34f248d1f928601D2E5246B | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x2859d961a6dBa6e7d30b2d383Af468edb4E7F4f6 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xaBaf56FC1bB6b4FF9fA4378C3C8723d2B2444324 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x0a7B96885b28deDE4a6887CA1150E36edb385BeE | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xDF1c35c3d5178e9d98043b35a6737Bd861c191c9 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x01e66950353400E93AEe7F041C0303103E2ef5Ab | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x862E3fe199723945a38871dE4F736f1233589CE5 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xFDC7BEc0aED8a584577fd59CbF56805eE8c976B3 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x5f525cd29377DC2155C2AbCDaC0Ce45e630318b7 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x3a69B365769a9dAb67865Ca5530c4B1D5259bCb7 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x97870c129abc9877b66534e49f152585D6Ca3655 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x1Ad061ad839f82C05767dACd2B5ab384E72B45a5 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xFd4b5ae43f2aA446b02209098438890d3998cC9F | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x732BF4fA8E57200621b0e1acbe8855c238823016 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xa76Fa6837A6ffc9F123F2193717A5965c68B0cbA | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xFaeb57c16D5E9A9f06c8c5dB12796f5a432Eb7d6 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xb501FDaad0F0863C4c72f7EB9Abc23965DCa973d | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x4C84a36fCDb7Bc750294A7f3B5ad5CA8F74C4A52 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x3dB4E0b1fC6072271BF51e9a0CC17E3c7C4C99f5 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| PTW - TU Darmstadt                  | 0x4A806a4851472F7cFd579d3FF5465F03c3c2B5d4 | Federator     | https://ptw.tu-darmstadt.euprogigant.io/sd/participant.json          |
| PTW - TU Darmstadt                  | 0x21CF19e1FaF3A62f82B432f82152e8c5C0FdBdaF | Federator     | https://ptw.tu-darmstadt.euprogigant.io/sd/participant.json          |
| PTW - TU Darmstadt                  | 0x9f4ceE0eBD03a1e9E4DcffaF876873d7a3e9595c | Federator     | https://ptw.tu-darmstadt.euprogigant.io/sd/participant.json          |
| PTW - TU Darmstadt                  | 0x6E7bec8715955B6Cc90A5A1767cd981b90C5a245 | Federator     | https://ptw.tu-darmstadt.euprogigant.io/sd/participant.json          |
| IONOS Cloud                         | 0x81336c245712DbF0E971de5463173bCaA9826d84 | Federator     | https://delta-dao.com/.well-known/participantIONOS.json              |
| Exoscale                            | 0x56eA3Cc92144Db3bA2bdE25131F40c7B98F7eD32 | Federator     | https://a1.digital.euprogigant.io/sd/participant.json                |
| Wobcom                              | 0x9Dc6aDA184fc98012D74F1C4f3f223183A4745D4 | Federator     | https://delta-dao.com/.well-known/participantWobcom.json             |
| Arsys                               | 0x0337b320DEfAddd9aDbC518f8A9cee30b606d15b | Federator     | https://arlabdevelopments.com/.well-known/ArsysParticipant.json      |
| Arsys                               | 0xD7b90d6476091F6bc4CAaC40180FB300351fAb9F | Federator     | https://arlabdevelopments.com/.well-known/ArsysParticipant.json      |
| State Library of Berlin             | 0xF20113edd04d98A64AD2A003B836677E1c9aACAD | Federator     | https://delta-dao.com/.well-known/participantStateLibraryBerlin.json |
| University of Lleida                | 0x62078F05Eb4450272D7E492F3660835826906822 | Federator     | https://delta-dao.com/.well-known/participantUniversitydeLleida.json |
| Software AG                         | 0xa702032E187E6A53EAddC28a735B414220712689 | Participant   | https://sagresearch.de/.well-known/participantSoftwareAG.json        |
| Software AG                         | 0x0a477f6297413f20C4fBc15F93e34dacE4136123 | Participant   | https://sagresearch.de/.well-known/participantSoftwareAG.json        |
| TU Wien                             | 0x586000e6DA330E140b11a4aeEbb81963d67F336b | Participant   | https://delta-dao.com/.well-known/participantTUWien.json             |
| Materna SE                          | 0xf596D17C4a3A5c92c4721627B9e5E5064651BF46 | Participant   | https://delta-dao.com/.well-known/participantMaterna.json            |
| Airbus                              | 0x3CEA8fBCbD1c745E081fD3937C18eE0b6Cc3f1b1 | Participant   | https://delta-dao.com/.well-known/participantAirbus.json             |
| OHB                                 | 0xF8dB4a6d529a14e3E029e2b8A9279f408909Fa20 | Participant   | https://delta-dao.com/.well-known/participantOHBdigital.json         |
| T-Systems Multimedia Solutions GmbH | 0x48535044200dAE3FD4f5b5C3f9b077fa5c230Ef3 | Participant   | https://delta-dao.com/.well-known/participantTsystemsMMS.json        |
| IKS – Gesellschaft für Informations- und Kommunikationssysteme mbH | 0x212c355c3ce41a272606da61F661dDd2b7F8a4B1 | Participant   | https://delta-dao.com/.well-known/participantIKS.json |
| msg David GmbH                      | 0x44C34FbBB727bDC648E65feCfF3FB9D4c85f1fe4 | Participant   | https://delta-dao.com/.well-known/participantMsgDavid.json           |
| Detecon                             | 0x8fBF860883BB71D691053A4363030Dc1c65f7017 | Participant   | https://delta-dao.com/.well-known/participantDetecon.json            |
| Perpetuum Progress                  | 0x2ee3c4F19f90237B7C45cfAD6B5dC4b5840563Ec | Participant   | https://delta-dao.com/.well-known/participantPerpetuumProgress.json  |
| SINTEF                              | 0x7DF1674a1e3449778eEB324652d3FF3Cb5046753 | Participant   |                                                                      |
| Peaq                                | 0xe3Df4851c094f5F6F1AC9AbfA4FC2075276195Ec | Participant   | https://delta-dao.com/.well-known/participantPeaq.json               |
| Datarella                           | 0xB21282F443EB0D490819d98F2976758af5C979B3 | Participant   | https://delta-dao.com/.well-known/participantDatarella2.json         |
| Datarella                           | 0x0aec046a558F13Ff18aAEc5E6f76084185358cdf | Participant   | https://delta-dao.com/.well-known/participantDatarella2.json         |
| Bosch                               | 0x6fE8aD445AD86b3d1325F79955Ef28d6e9cb2258 | Participant   | https://delta-dao.com/.well-known/participantBosch.json              |
| TU Dortmund                         | 0x51Decd187744bCfAD1BAb0A3E71dD68fAC0ba478 | Participant   |                                                                      |
| RWS                                 | 0xa98A6eefbAE870b88a9C7A43f4b50066A01c93b6 | Participant   | https://delta-dao.com/.well-known/participantRijkswaterstaat.json    |
| Craftworks                          | 0x9dfbda23b65efB1c836828D74a96eB8528A60f3C | Participant   | https://craftworks.euprogigant.io/sd/participant.json                |
| Concircle Österreich GmbH           | 0xb2AF8b92bFaC5299Cb6EDEf16150BFD1d4d26a93 | Participant   | https://delta-dao.com/.well-known/participantConcircleAustria.json   |
| DENSO AUTOMOTIVE Deutschland GmbH   | 0x2b92BF0496B7B41ea2d723325DDE96651795c784 | Participant   | https://delta-dao.com/.well-known/participantDenso.json              |
| DENSO AUTOMOTIVE Deutschland GmbH   | 0xe761F8e33c71D08A9323Cb2c711aB4Fae2634276 | Participant   | https://delta-dao.com/.well-known/participantDenso.json              |
| Struggle Creative Oy                | 0x895975869261A215813e33568a295F94A3F301ed | Participant   | https://delta-dao.com/.well-known/participantStruggle.json           |
| Sphereon                            | 0xDa4fc9E82Ac4E44207a1f74137493D3437D80761 | Participant   | https://delta-dao.com/.well-known/participantSphereon.json.          |
| Austrian Institute of Technology    | 0xfc739f2F91921eb710878ad2Ca38C147a784C96f | Participant   | https://delta-dao.com/.well-known/participant.json                   |
| acatech                             | 0xF62bF6371Ee020Cb2164Ac3C338514DBbb93A0D4 | Participant   | https://delta-dao.com/.well-known/participantAcatech.json            |
| Spanish Ministry of Economic Affairs and Digital Transformation| 0x6E1cE3530A12F89cF567788C132454E5dC7D3cCE |Participant| https://delta-dao.com/.well-known/participantspanishministryofeconomicaffairs.json.json |
| ATOS                                | 0x943CaA8afCAdd2F64a7cE9E53A91d5ea0BEb40c1 | Participant   | https://delta-dao.com/.well-known/participantATOS.json               |
| ATOS                                | 0x7A6246e02B2aA276203469Cfb839a2666520D8b5 | Participant   | https://delta-dao.com/.well-known/participantATOS.json               |
| TU Delft                            | 0x9391291b0Df512B20810183744De8272774b6655 | Participant   | https://delta-dao.com/.well-known/participantTUDelft.json            |
| Bigchain DB                         | 0x61DB12d8b636Cb49ea09eCa58a893dA9480E1F33 | Participant   | https://delta-dao.com/.well-known/participantBigchainDB.json         |
| Perpetuum Progress GmbH             | 0x2ee3c4F19f90237B7C45cfAD6B5dC4b5840563Ec | Participant   | https://delta-dao.com/.well-known/participantPerpetuumProgress.json  |
| Mercedes-Benz Singapore Pte. Ltd.   | 0x203C7AA993EED06932FA327a192de9A8370b5Ab4 | Participant   | https://www.delta-dao.com/.well-known/participantMercedesBenzLtd.json |
| Mercedes-Benz Singapore Pte. Ltd.   | 0x4d6240C7Ef355a2E85c13B26A49A35908ce853E5 | Participant   | https://www.delta-dao.com/.well-known/participantMercedesBenzLtd.json |
| Höchstleistungsrechenzentrum Stuttgart (HLRS) | 0x6bF77769e84045a9EAC64573e70a5562457C52ad | Participant | https://delta-dao.com/.well-known/participantHLRS.json        |
| Ruhr-Universität Bochum             | 0x17c8D253443F9E7305A2539d7aF177B21aAD3355 | Participant   | https://delta-dao.com/.well-known/participantRuhrUniversityBochum.json |
| north.io GmbH                       | 0xFDF411B7A23182e7F0a635bdF0d25f0fCb2aAf74 | Participant   | https://delta-dao.com/.well-known/participantNorthIO.json             |
| CONTACT Software GmbH               | 0x3560626F234eD181E807E4e31ded56D9aca1ac58 | Participant   | https://delta-dao.com/.well-known/participantContactSoftware.json     |
| Universität Siegen                  | 0xF0926FbE8e60E54aFB4fD296B2698230ab32799b | Participant   |  https://delta-dao.com/.well-known/participantUniversitaetSiegen.json |
| AWS-Institut für digitale Produkte und Prozesse gGmbH | 0xAA782a260Ad773bca5Ff0535356CB0F7B94Cd254 | Participant | https://delta-dao.com/.well-known/participantAWSi.json |
| 5D Institut GmbH                    | 0x2aC6802160A74677B7cEC1aaD7E41Ec968D57896 | Participant   | https://delta-dao.com/.well-known/participant5DInstitut.json          |
| Accenture                           | 0xFd1BEC7E551fAeA6102045D720dD693c4e9C8E06 | Participant   | https://delta-dao.com/.well-known/participantAccenture.json           |
| Airbus Defence and Space GmbH       | 0xa2199E3f60fC244037Efd5A77714CC05F604F855 | Participant   | https://delta-dao.com/.well-known/participantAIRBUS_DEFENCE_AND_SPACE_GMBH.json |
| Bernard Technologies GmbH           | 0x5101ea56E29f5dc03285809b6157f0588ff255D0 | Participant   | https://delta-dao.com/.well-known/participantBernardTechnologies.json  |
| Deutsches Forschungszentrum für Künstliche Intelligenz GmbH | 0x8B7f2b75B7F87D3125C8B0eDB85639B441BBcE21 | Participant   | https://delta-dao.com/.well-known/participantDFKI.json  |
| Hochschule für angewandte Wissenschaften Kempten | 0xb11124Dfa40E44b3283068fd07bf6FdE60caf06A | Participant   | https://delta-dao.com/.well-known/participantHSKempten.json |
| Fraunhofer-Institut für Graphische Datenverarbeitung IGD | 0x632460b14aDd90aD9430e381B4662779cC1ab7a6 | Participant   | https://delta-dao.com/.well-known/participantFraunhoferIGD.json  |
| 52°North GmbH                       | 0x1f65110b63B6044f1E92543C09231842131798C7 | Participant   | https://delta-dao.com/.well-known/participant52north.json                |
| Atos Information Technology GmbH    | 0x7A6246e02B2aA276203469Cfb839a2666520D8b5 | Participant   | https://delta-dao.com/.well-known/participantAtosInformationTechnology.json  |
| TrueOcean GmbH                      | 0xDFa29AE20eac7f203DdDbe15E1830985e99143B8 | Participant   | https://delta-dao.com/.well-known/participantTrueOcean.json              |
| Institute for Language and Speech Processing | 0xFfA05d656465568BE83B11bf274c5458AC8401AC | Participant   | https://delta-dao.com/.well-known/participantILSP.json          |
| Vicomtech                           | 0xb500BfE3d89b5D6b0d2b91841c3A3aD568Cb0FdC | Participant   | https://delta-dao.com/.well-known/participantVicomtech.json              |
| Fraunhofer IAIS                     | 0x8BF36BEFC22a7b9c1a546139bFd4ae8420bcFf0e | Participant   | https://delta-dao.com/.well-known/participantFraunhoferIAIS.json         |
| Berger Holding GmbH & Co. KG        | 0x2dB30B996C0E2990F836685Cf1A2939b3299f8e5 | Participant   | https://delta-dao.com/.well-known/participantBergerHolding.json          |
| Christoph Kroschke GmbH             | 0x224482ebcf914b9FA9E312036B377e26B676E534 | Participant   | https://kroschke.de/.well-known/participantKroschke.json                 |
| Brinkhaus GmbH                      | 0xD580c01E2f503287006138a94eBBc537Fe7eBD25 | Participant   | https://delta-dao.com/.well-known/participantBrinkhausGmbH.json          |
| Gühring KG                          | 0x4B107057aB8278c7d9436bf76230d16e5F7BaD16 | Participant   | https://delta-dao.com/.well-known/participantGuehring.json               |
| Fraunhofer-Institut für Werkzeugmaschinen und Umformtechnik IWU | 0x7bf493b142AB0bb37c7f766A1585245901891685 | Participant   | https://delta-dao.com/.well-known/participantFraunhoferIWU.json  |
| imc information multimedia communication AG | 0x1c0c9211E8Ec8E0253A53880D5481e4580B62125 | Participant   | https://delta-dao.com/.well-known/participantIMC.json            |
| SAP Fioneer GmbH                    | 0xEEe803bEFd2B4f229E57523Edb11CDE38DD1a23E | Participant   | https://delta-dao.com/.well-known/participantSAPFioneer.json             |
| NT Neue Technologie AG              | 0xb828bA1850aA11daA1890896573Aa6008221A671 | Participant   | https://delta-dao.com/.well-known/participantNeueTechnologie.json        |
| IONOS SE                            | 0x005d24698bF41c398ECF15a93455621932a6e19F | Participant   | https://delta-dao.com/.well-known/participantIONOS2.json                 |
| ScopeSET GmbH                       | 0x746d4715c24fc4d26D02A558ACF98dC717C68E1e | Participant   | https://scopeset.de/.well-known/participantScopeSET.json                 |
| RIB Software GmbH                   | 0x1Bf21DCb771Aba18B1D23AA6D8a619C1AB1811a4 | Participant   | https://delta-dao.com/.well-known/participantRIBSoftware.json            |
| msg DAVID GmbH                      | 0x04FEA446847c3539d35Cca0a74Cb82Da811BAfc3 | Participant   | https://delta-dao.com/.well-known/participantmsgDAVID_2.json             |
| Arvato Systems GmbH                 | 0x69bF63B2Bb6A93fc4ff434595A72a4ED313E5698 | Participant   | https://delta-dao.com/.well-known/participantArvatoSystemsGmbH.json      |
| Fraunhofer-Institut für Produktionsanlagen und Konstruktionstechnik | 0xEdfd506dd449Cd06c91f51Fe9DfE4e3E57B2F8f5 | Participant   | https://delta-dao.com/.well-known/participantFHGIPK.json  |
| OSISM GmbH                          | 0x0763BfBcBfA0126b5A5509fB1185b7b6476BdAd8 | Participant   | https://delta-dao.com/.well-known/participantOSISM.json                  |
| Netcompany-Intrasoft S.A.           | 0x54d2946677CC16E06Efd6161A4abFA17fc98Afc3 | Participant   | https://delta-dao.com/.well-known/participantNetcompanyInfrasoft.json    |
| Stackable GmbH                      | 0x5880C2C30C922FE700fc079e1b6BBa7e9E7DE577 | Participant   | https://delta-dao.com/.well-known/participantStackable.json              |
| FZI Forschungszentrum Informatik    | 0xc2350eA5913511A95c1aBED51de377A0b92846Be | Participant | https://delta-dao.com/.well-known/participantFZI.json                      |
| ProCarement GmbH                    | 0x0c85Cd08E6643Fa3E4B75268431d19CcFC99C916 | Participant   | https://delta-dao.com/.well-known/participantProCarement.json            |
| Hochschule Furtwangen University (HFU) | 0x1153265057782e8C57292CA590E50acC36037204 | Participant   | https://delta-dao.com/.well-known/participantFurtwangenUniversity.json |
| Daten-Kompetenzzentrum Städte und Regionen DKSR GmbH | 0xF211efa0C51559e6730db3Ba6FE1f1D46A68BE14 | Participant   | https://delta-dao.com/.well-known/participantDKSR.json  |
| GMN Paul Müller Industrie GmbH & Co. KG | 0x7209bd8fDd841358a3CF9E7DaD8D9dCe2E4BbBB8 | Participant   | https://delta-dao.com/.well-known/participantGMN.json                |
| Fraunhofer-Institut für Offene Kommunikationssysteme FOKUS | 0xDB5807EacA2937f6264c5725538f8Ec357b4d3b2 | Participant   | https://delta-dao.com/.well-known/participantFraunhoferFokus.json |
| Bechtle Aktiengesellschaft          | 0x8482256AC35fcA568a53CfD77Af9538FEC0691bb | Participant   | https://delta-dao.com/.well-known/participantBechtleAktiengesellschaft.json |
| Bundesdruckerei Gruppe GmbH         | 0x985f314171DFc0Ec3443E32b262c3135E094eD72 | Participant   | https://delta-dao.com/.well-known/participantBundesdruckerei.json        |
| Fraunhofer IOSB                     | 0x99c030936B5E7381E65B645d3762A93147EB15F7 | Participant   | https://delta-dao.com/.well-known/participantFraunhoferIOSB-AST.json     |
| embeteco GmbH & Co. KG              | 0x7104a77Ca5FfC6D3f0840048C307d05EA3b529C0 | Participant   | https://delta-dao.com/.well-known/participantEmbeteco.json               |
| T-Systems International GmbH        | 0x9c373e9f125497281f37AeF603fa99572856Bc38 | Participant   | https://delta-dao.com/.well-known/participantTSystemsInternational.json  |
| IPROconsult GmbH                    | 0x8FAF0702C51c94b5848774129047d75cEe49EE87 | Participant   | https://delta-dao.com/.well-known/participantIPROconsult.json            |
| Elektra Solar GmbH                  | 0x3EAbA16E4Ac451D85839A42eb9e7C61F157C88b7 | Participant   | https://delta-dao.com/.well-known/participantElektroSolar.json           |
| Setlabs Research                    | 0x1c99F7C29EE0e79CAAD8E4d0Cc0b95D5Ece62294 | Participant   | https://delta-dao.com/.well-known/participantSetlabs.json                |
| Schüßler-Plan Digital GmbH          | 0xb9C596E9eC598a865b51f3F53ae7d122B7b7a937 | Participant   | https://delta-dao.com/.well-known/participantSchuesslerPlanDigitalGmbH.json |
| Data Machine Intelligence Solutions GmbH | 0xb7cF56a08F2B6ccF250B431125850968b7f6a950 | Participant   | https://delta-dao.com/.well-known/participantDMIS.json              |
| Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR) | 0x4476123c4B4706cf88CbfA055b72726Baa1e8041 | Participant   | https://delta-dao.com/.well-known/ParticipantDLR.json  |
| OHB System AG                       | 0x9309Ce467475DbB0a9c549B988F6571EB024507C | Participant   | https://delta-dao.com/.well-known/participantOHB.json                    |
| C&S Computer und Software GmbH      | 0xb51d556E910Dd1887602034bbB66DA63EaA80ce2 | Participant   | https://delta-dao.com/.well-known/participantComputerUndSoftware.json    |
| eco - Verband der Internetwirtschaft e.V. | 0x007dB3DC8De9ae0F8AfeeBf1f7C92CcbD1A75Fd7 | Participant   | https://delta-dao.com/.well-known/participantEco.json              |
| Institut für Automation und Kommunikation e. V. | 0xe70bBA7bC033Bf1Ce6Fa3328eCFAAc8966E66966 | Participant   | https://delta-dao.com/.well-known/participantIfak.json       |
| Fujitsu Services GmbH               | 0xE4EE92b3a6B661b7148305Fa3A8d96062CBFAFc5 | Participant   | https://delta-dao.com/.well-known/participantFujitsuServices.json        |
| RADIUSMEDIA KG                      | 0x37e01308d6A0E322dECc457a13E0B2b2086D84B1 | Participant   | https://delta-dao.com/.well-known/participantRadiusMedia.json            |
| ahu GmbH Wasser Boden Geomatik      | 0x9Adf8e343ec1C7dB2B44e420bB8F4Cc51dEbFb7a | Participant   | https://delta-dao.com/.well-known/participantAhu.json                    |
| Hochschule Offenburg (HSO)          | 0xE64872A181F0695DA0660fE0B809a89A3bA359AA | Participant   | https://delta-dao.com/.well-known/participantHSO.json                    |
| FeltLabs                            | 0x533d456D3D5c16E6390647E2167678b7a76A4840 | Participant   | https://delta-dao.com/.well-known/participantFeltLabs.json               |
| FeltLabs                            | 0x56e194D46fF305560f51D06cE84649C1DD91d2F8 | Participant   | https://delta-dao.com/.well-known/participantFeltLabs.json               |
| BigchainDB GmbH                     | 0x61DB12d8b636Cb49ea09eCa58a893dA9480E1F33 | Participant   | https://delta-dao.com/.well-known/participantBigchainDB.json             |
| Deal ex Machina                     | 0xC8a08b33995594bfdB0ef9c18EB72da0469E396F | Participant   | https://delta-dao.com/.well-known/participantDealExMachina.json          |
| Deloitte                            | 0x289Ff19C1e544B6E9488d5E79966491A2bAa88C9 | Participant   | https://delta-dao.com/.well-known/participantDeloitte.json.              |
| 3D Mapping Solutions GmbH           | 0x2650e382770A04bE0f7E362ed578FB261A60F4b3 | Participant   | tbd                                                                      |
| Exaion SAS           | 0x7244dF59313D6fc05C9a95E62486340205195Efc | Participant   | https://delta-dao.com/.well-known/participantExaion.json                                                                      |
